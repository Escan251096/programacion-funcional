//Programación convencional
const numbers = [1, 2, 3, 4];
let doubled = [];

for (let i = 0; i < numbers.length; i++) {
  doubled.push(numbers[i] * 2);
}

console.log("Arreglo [1,2,3,4] con valores al doble", doubled) //==> [2, 4, 6, 8]

//Programación funcional

// Antes de entrar en detalles, no está de más aclarar que la Programación Funcional (Functional Programming o FP) 
// es un “paradigma de programación”. Esto quiere decir que es una forma de pensar en cómo se organiza un programa basado
// en una serie de principios (que mencionaremos más abajo). Como comparación, 
// los paradigmas predominantes son la Programación Orientada a Objetos (OOP — Object Oriented Programming) 
//y Programación por Procedimientos (Procedural Programming).

const fnumbers = [1, 2, 3, 4];
const fdoubled = fnumbers.map(n => n * 2);

console.log("Arreglo [1,2,3,4] con valores al doble usando programación funcional", fdoubled) //==> [2, 4, 6, 8];

//En ambos ejemplos nuestro objetivo era obtener el doble de cada valor de un array. En la primera versión imperativa, usamos un contador
// que es nuestro estado de recorrido del primer array para saber dónde insertar el doble de cada valor en el segundo array. 
//¿Cuántas veces habrá causado un bug ese maldito contador?.
//Con la manera declarativa nos olvidamos del contador y nos centramos en especificar la función que queremos aplicar. 
//Es decir, cuando usamos programación declarativa, las sentencias declaran QUÉ hacer, delegando en otra función, 
//pero no CÓMO hacerlo, a diferencia de la programación imperativa. Interesante, ¿verdad?. 

//Inmutabilidad

let foo = [1, 2, 3];
let bar = foo;
bar.push(10000);

console.log("Arreglo [1,2,3] que no debió ser cambiado", foo, "Y el que si", bar) // ==> [1, 2, 3, 10000]

//Solucion

/*import {
  append
} from 'ramda';*/
const foo2 = [1, 2, 3];
//const bar2 = append(4, foo2);

console.log("Arreglo usando ramda", foo2); // ==> [1, 2, 3]

//Función Pura

// Una función pura es aquella cuyo resultado será siempre el mismo para un mismo valor de entrada, es decir, 
// es determinista y sólo depende del argumento recibido; tiene transparencia referencial. 
// Además no tiene efectos colaterales (no modifica ninguna variable global ni local).

function appendSumOfValues(entryArray) {
  const total = entryArray.reduce((accummulator, currentValue) => accummulator + currentValue);
  const results = entryArray.concat(total);

  return results;
}

const originalSum = [3, 2]
console.log(appendSumOfValues(originalSum));

//Funciónes de order superior.

// Las funciones de orden superior son aquellas que reciben una o más funciones como argumento o bien devuelven 
// funciones como resultado. Nos interesan porque nos permiten reutilizar la forma de ejecutar otras funciones. 
// En especial nos serán muy útiles map, filter y reduce, los pilares de la programación funcional en JavaScript,
// su interés principal está en usarlo sobre arrays.

[1, 2, 3].map(n => n + 1); // => [2, 3, 4]

[1, 2, 3].filter(n => n > 1); // => [2, 3]

[1, 2, 3].reduce((acc, n) => acc + n, 0);
// 0 + 1 => 1
// 1 + 2 => 3
// 3 + 3 => 6
// => 6

//Decorador unario

//Un decorador unario convierte cualquier función en una función unaria, es decir, una función que sólo tiene un parámetro de entrada.

//Junto con estas funciones es interesante presentar el decorador unario. Veamos un ejemplo:

//Vamos a pasar un array de strings a un array de números, para eso utilizaremos map 
// (aplica una función a cada elemento de un array) y parseInt (convierte de string a número). 
//Esto lo podemos hacer de la siguiente manera:

const resultU = ["1", "2", "3"].map((item) => parseInt(item));
console.log(resultU); // [1, 2, 3]

["1", "2", "3"].map(parseInt); // => [1, NaN, NaN]

//["1", "2", "3"].map(unary(parseInt)); //=> [1, 2, 3]

//Currying

//Currificar consiste en convertir una función de múltiples variables en una secuencia de  funciones unarias
// Esto hace que, si la función tiene N argumentos de entrada, nunca se ejecutará si no le proporcionamos todos los argumentos de entrada que pide, al contrario de lo que ocurre por defecto en JavaScript (como con parseInt, que podíamos ejecutarla con un sólo argumento a pesar de recibir dos).
// La currificación nos permite reutilizar una función en diferentes sitios con diferentes configuraciones. 

//Sin Currying:

const sumaSinCurry = (a, b) => a + b;

//De manera que:
sumaSinCurry(3, 5); //=> 8
//sumaSinCurry(3)(5); //=> TypeError

//Si currificamos la función nos quedaría así: (ES6)

const sumaConCurryES6 = (a) => (b) => a + b;

// la misma función con sintaxis de ES5 quedaría de la siguiente manera:
function sumaConCurry(a) {
  return function (b) {
    return a + b;
  }
}

sumaConCurry(3)(5); //=> 8
sumaConCurryES6(3)(5);

const sumPending = sumaConCurry(3); // (b) => a + b
sumPending(2); // 8

/* La composición

Una vez que tenemos nuestras funciones puras, la composición nos permite aplicar dichas funciones en cadena. 
Una manera sencilla de usar composición sería, una vez más, usando Ramda. 
Para ello esta librería nos proporciona dos funciones: compose (que ejecuta las funciones de derecha a izquierda), 
y pipe (que ejecuta las funciones de izquierda a derecha). 
¿ En que consiste eso? En que la salida de una función sirve como salida para la siguiente, 
permitiendonos crear un código más simple y legible. */

// Usando ES6

const originalES6 = [80, 3, 14, 22, 30];

const result = originalES6
  .filter((value) => value % 2 === 0)
  .filter((value) => value > 20)
  .reduce((accumulator, value) => accumulator + value);

console.log(result); // 132

/*Usando ramda

const originalRamda = [80, 3, 14, 22, 30];

// Vamos a usar  Pipe esta función nos permite ir encadenando funciones, la salida de una sirve como entrada para la otra.
// La función pipe nos permite pasar tantos callbacks y parámetros de entrada como queramos.

const resultRamda = R.pipe(
  R.filter((value) => value % 2 === 0),
  R.filter((value) => value > 20),
  R.sum,
)(originalRamda);

console.log(resultRamda); // 132*/